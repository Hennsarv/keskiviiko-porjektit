﻿
using System;
using System.Data;
using System.Globalization;
using System.Numerics;

// hennu esimene konsool

class Program // see on klass 
{
    public static DateTime algus = DateTime.Today;

 
    static void Main()
    {
        // mulle tundub, et enne blokke käime (alguseks) üle teisendamised
        // te kuidagi ise ronisite selle teema peale

        //Console.Write("Anna üks arv: ");
        //var arv = decimal.Parse(Console.ReadLine());
        //Console.WriteLine($"sinu arvu ruut on {arv*arv}");

        int i = 4000;
        short s = (short)(i*i);
        Console.WriteLine(s);

        double d = 4.578e+250; // 4.578 * 10^10
        float f = (float)d;

        Console.WriteLine(d);

        char a = 'A';
        a += (char)1;
        a = (char)(a + 1);

        i = a;
        d = i;

        Console.Write("Anna üks arv: ");
        string ss = Console.ReadLine();

        //double dd = double.Parse(ss, System.Globalization.CultureInfo.GetCultureInfo("en-us").NumberFormat);
        double dd = Convert.ToDouble(ss, CultureInfo.GetCultureInfo("en-us").NumberFormat);
        
        
        Console.WriteLine(dd.ToString(CultureInfo.GetCultureInfo("en-us").NumberFormat));

        var ci_fin = CultureInfo.GetCultureInfo("fi-fi");
        var dtf_fin = ci_fin.DateTimeFormat;

        Console.Write("millal sa sündinud oled: ");
        DateTime synd = DateTime.Parse(Console.ReadLine(), dtf_fin);
        Console.WriteLine(synd.ToString("(dddd) dd MMM yyyy",dtf_fin));

        // kõigil klassidel - NB! KÕIGIL on olemas teisendus stringiks ToString()
        // ToString() - teisenda lihtsalt stringiks
        // ToString("formaat") - teisenda stringiks sellises formaadis (ainult arvudel ja kuupäevadel)
        //                       ehk klasssidel, mis on IFormatable (kunagi räägime, mis see on)
        // ToString(CultureInfo.GetCultureInfo("de-DE").NumberFormat) - kasuta sellist 'lokaati'
        // ToString("Formaat", CultureInfo...) - kasuta formaati ja 'lokaati'

        // selle ToStringiga saame veel oma tehtud andmetüüpidel mängida - las jääda tulevikku

        Console.WriteLine($"sa oled elanud {(DateTime.Today-synd).Days} päeva");

        DateTime mingi = new DateTime(1955, 3, 7, 10, 0, 0); // datetime konstrueerimine numbritest
        DateTime teine = DateTime.Parse("7. märts 1955 10:00"); // datetime teisendamine tekstist

        DateTime täna = DateTime.Today;   // tänane päev
        DateTime praegu = DateTime.Now;   // praegune hetk - mis erinevus

        TimeSpan eluaeg = DateTime.Today - synd;
        Console.WriteLine(eluaeg.Seconds);          // arvutab ajavahe sekundites (int)
        Console.WriteLine(eluaeg.TotalSeconds);     // arvutab ajavahe sekundites (double)

        Console.WriteLine(eluaeg);

        BigInteger bi = BigInteger.Parse(new string('1', 1000));
        Console.WriteLine($"suur arv {bi}");
        Console.WriteLine($"suure arvu ruut on {bi*bi}");


        


    }



}

